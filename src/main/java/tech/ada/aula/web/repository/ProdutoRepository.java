package tech.ada.aula.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tech.ada.aula.web.model.entity.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

	@Query(value="SELECT p FROM Produto p WHERE UPPER(p.nome) LIKE CONCAT('%',UPPER(:nome),'%')")
	List<Produto> findByNome(@Param("nome") String nome);
}
