package tech.ada.aula.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.ada.aula.web.model.dto.CategoriaProdutoDTO;
import tech.ada.aula.web.service.CategoriaProdutoService;

@RestController
@RequestMapping("/categorias")
public class CategoriaProdutoController extends BaseController<CategoriaProdutoDTO, CategoriaProdutoService> {

	public CategoriaProdutoController(CategoriaProdutoService service) {
		super(service);
	}

}
