package tech.ada.aula.web.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import tech.ada.aula.web.model.dto.ProdutoDTO;
import tech.ada.aula.web.service.ProdutoService;

@RestController
@RequestMapping("/produtos")
@Slf4j
public class ProdutoController extends BaseController<ProdutoDTO, ProdutoService> {

	public ProdutoController(ProdutoService service) {
		super(service);
	}
	
	@PostMapping("/list")
    public ResponseEntity<List<ProdutoDTO>> criar(@RequestBody List<ProdutoDTO> entidades) {
        try {

            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(service.criarVarios(entidades));

        }catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
	
	@PostMapping("/filter")
    public ResponseEntity<List<ProdutoDTO>> filtrar(@RequestBody ProdutoDTO entidade) {
        try {
        	
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(service.filter(entidade));

        }catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
	
	@GetMapping("/filter/{nome}")
    public ResponseEntity<List<ProdutoDTO>> buscarUm(@PathVariable("nome") String nome) {
        try {
        	
        	return ResponseEntity.ok(service.buscarPorNome(nome));
        	
        }catch(EntityNotFoundException ex) {
        	
        	return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }catch(Exception ex) {
        	
            log.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

}
