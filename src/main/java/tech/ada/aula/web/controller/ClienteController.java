package tech.ada.aula.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.ada.aula.web.model.dto.ClienteDTO;
import tech.ada.aula.web.service.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteController extends BaseController<ClienteDTO,ClienteService> {

	public ClienteController(ClienteService service) {
		super(service);
	}

}
