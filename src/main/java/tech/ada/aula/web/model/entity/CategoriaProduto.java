package tech.ada.aula.web.model.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "categoria_produto")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoriaProduto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="nome", nullable=false, unique=true)
	private String nome;
	
	@Column(name="descricao", nullable=false)
	private String descricao;
	
	@OneToMany(mappedBy = "categoriaProduto")
	private List<Produto> produtos;
}
