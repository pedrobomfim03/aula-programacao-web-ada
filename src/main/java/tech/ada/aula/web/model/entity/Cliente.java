package tech.ada.aula.web.model.entity;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="clientes")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cliente {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="nome", nullable=false)
	private String nome;
	
	@Column(name="cpf", nullable=false, unique=true)
	private String cpf;
	
	@Column(name="dataNascimento", nullable=false)
	private LocalDate dataNascimento;
	
	@Column(name="cep", nullable=true)
	private String cep;
	
	@Column(name="uf", nullable=true)
	private String uf;
	
	@Column(name="municipio", nullable=true)
	private String municipio;
	
	@Column(name="bairro", nullable=true)
	private String bairro;
	
	@Column(name="localidade", nullable=true)
	private String localidade;
	
	@Column(name="complemento", nullable=true)
	private String complemento;
	
	/*@ManyToMany(mappedBy = "clientes")
	private List<Produto> produtos;*/
}
