package tech.ada.aula.web.model.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import tech.ada.aula.web.model.dto.UsuarioLoginDTO;
import tech.ada.aula.web.model.entity.Usuario;

@Mapper(componentModel = "spring")
public interface UsuarioMapper {
	List<UsuarioLoginDTO> parseListDTO(List<Usuario> clientes);
	List<Usuario> parseListEntity(List<UsuarioLoginDTO> clientes);
	@Mapping(target = "password", ignore=true)
	@Mapping(source = "perfil.id",target = "perfil")
	UsuarioLoginDTO parseDTO(Usuario cliente);
	@Mapping(target="authorities",ignore=true)
	@Mapping(source = "perfil",target = "perfil.id")
	Usuario parseEntity(UsuarioLoginDTO cliente);
}
