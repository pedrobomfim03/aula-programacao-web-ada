package tech.ada.aula.web.model.dto;

import java.time.LocalDate;

import org.hibernate.validator.constraints.br.CPF;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClienteDTO{
	
		private Long id;
		private String nome;
		@CPF(message="CPF com formato inválido.")
		private String cpf;
		private String cep;
		private String complemento;
		private String uf;
		private String localidade;
		private String municipio;
		private String bairro;
		private LocalDate dataNascimento;
}
