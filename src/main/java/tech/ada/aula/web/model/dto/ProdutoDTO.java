package tech.ada.aula.web.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProdutoDTO{
		private Integer id;
		@NotNull(message="O campo categoria produto deve estar preenchido") 
		private CategoriaProdutoDTO categoriaProduto;
		@NotBlank(message="O campo nome deve estar preenchido") 
		private String nome;
		@NotNull(message="O campo preco deve estar preenchido") 
		private Double preco;
		@NotNull(message="O campo quantidade estoque deve estar preenchido") 
		private Integer quantidadeEstoque;
		String descricao;
}
