package tech.ada.aula.web.model.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import tech.ada.aula.web.model.dto.CategoriaProdutoDTO;
import tech.ada.aula.web.model.entity.CategoriaProduto;

@Mapper(componentModel = "spring")
public interface CategoriaProdutoMapper {
	List<CategoriaProdutoDTO> parseListDTO(List<CategoriaProduto> categorias);
	List<CategoriaProduto> parseListEntity(List<CategoriaProdutoDTO> categorias);
	CategoriaProdutoDTO parseDTO(CategoriaProduto categoria);
	CategoriaProduto parseEntity(CategoriaProdutoDTO categoria);
}
