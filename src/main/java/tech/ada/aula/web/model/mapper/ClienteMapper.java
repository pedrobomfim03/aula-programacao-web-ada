package tech.ada.aula.web.model.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import tech.ada.aula.web.model.dto.ClienteDTO;
import tech.ada.aula.web.model.entity.Cliente;

@Mapper(componentModel = "spring")
public interface ClienteMapper {
	List<ClienteDTO> parseListDTO(List<Cliente> clientes);
	List<Cliente> parseListEntity(List<ClienteDTO> clientes);
	ClienteDTO parseDTO(Cliente cliente);
	Cliente parseEntity(ClienteDTO cliente);
}
