package tech.ada.aula.web.model.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import tech.ada.aula.web.model.dto.ClienteDTO;
import tech.ada.aula.web.model.entity.Cliente;

@Component
public class ClienteMapperOld {
	
	public List<ClienteDTO> parseListDTO(List<Cliente> clientes){
		return clientes.stream()
		.map(cliente->parseDTO(cliente))
		.toList();
	}
	
	public ClienteDTO parseDTO(Cliente cliente) {
		/*return new ClienteDTO(
				cliente.getId(),
				cliente.getNome(),
				cliente.getCpf(),
				cliente.getDataNascimento()
		);*/
		return null;
	}
	
	public List<Cliente> parseListEntity(List<ClienteDTO> clientes){
		return clientes.stream()
		.map(cliente->parseEntity(cliente))
		.toList();
	}
	
	public Cliente parseEntity(ClienteDTO cliente) {
		/*return new Cliente(
				cliente.id(),
				cliente.nome(),
				cliente.cpf(),
				cliente.dataNascimento()
		);*/
		return null;
	}
}
