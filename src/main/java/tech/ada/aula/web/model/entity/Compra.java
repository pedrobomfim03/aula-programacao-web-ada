package tech.ada.aula.web.model.entity;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="compra")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Compra {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@JoinColumn(name = "produto_id", nullable = false)
	@ManyToOne()
	private Produto produto;
	
	@JoinColumn(name = "cliente_id", nullable = false)
	@ManyToOne()
	private Cliente cliente;
	
	@Column(name="data", nullable=false)
	private LocalDateTime data;
	
	@Column(name="preco", nullable=false)
	private Double preco;
	
	@Column(name="quantidade", nullable=false)
	private Integer quantidade;
}
