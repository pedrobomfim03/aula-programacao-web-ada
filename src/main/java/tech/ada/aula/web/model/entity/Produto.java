package tech.ada.aula.web.model.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "produto")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Produto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@JoinColumn(name = "categoria_produto_id", nullable = false)
	@ManyToOne(fetch = FetchType.EAGER)
	private CategoriaProduto categoriaProduto;
	
	@Column(name="nome", nullable=false, unique=true)
	private String nome;
	
	@Column(name="preco", nullable=false)
	private Double preco;
	
	@Column(name="quantidade_estoque", nullable=false)
	private Integer quantidadeEstoque;
	
	@Column(name="data_ultima_atualizacao", nullable=false)
	@UpdateTimestamp
	private LocalDateTime dataUltimaAtualizacao;
	
	@Column(name="descricao", nullable=true)
	private String descricao;
	
	
	/*
	@ManyToMany()
	@JoinTable(
			name="compra",
			joinColumns = {@JoinColumn(name = "produto_id")},
			inverseJoinColumns = {@JoinColumn(name="cliente_id")})
	private List<Cliente> clientes;*/
}
