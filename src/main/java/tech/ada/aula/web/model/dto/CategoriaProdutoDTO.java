package tech.ada.aula.web.model.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CategoriaProdutoDTO{
	
		private Integer id;
		@NotBlank(message="O campo nome deve estar preenchido") 
		private String nome;
		@NotBlank(message="O campo descrição deve estar preenchido") 
		private String descricao;
}
