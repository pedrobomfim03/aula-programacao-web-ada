package tech.ada.aula.web.model.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import tech.ada.aula.web.model.dto.ProdutoDTO;
import tech.ada.aula.web.model.entity.Produto;

@Mapper(componentModel = "spring")
public interface ProdutoMapper {
	List<ProdutoDTO> parseListDTO(List<Produto> produtos);
	List<Produto> parseListEntity(List<ProdutoDTO> produtos);
	ProdutoDTO parseDTO(Produto produto);
	@Mapping(target = "dataUltimaAtualizacao", ignore = true)
	@Mapping(target = "categoriaProduto.produtos", ignore = true)
	Produto parseEntity(ProdutoDTO produto);
}
