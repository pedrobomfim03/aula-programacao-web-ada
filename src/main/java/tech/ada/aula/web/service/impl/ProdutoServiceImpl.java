package tech.ada.aula.web.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import tech.ada.aula.web.model.dto.ProdutoDTO;
import tech.ada.aula.web.model.entity.Produto;
import tech.ada.aula.web.model.mapper.ProdutoMapper;
import tech.ada.aula.web.repository.ProdutoFilterRepository;
import tech.ada.aula.web.repository.ProdutoRepository;
import tech.ada.aula.web.service.ProdutoService;

@Service
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepository repository;
	
	@Autowired
	private ProdutoFilterRepository filterRepository;
	
	@Autowired
	private ProdutoMapper mapper;
	
	@PersistenceContext
	private EntityManager em;
	
	public List<ProdutoDTO> buscarTodos() {
		return mapper.parseListDTO(repository.findAll());
	}
	
	public List<ProdutoDTO> filter(ProdutoDTO produtoDTO) {
		Produto produto = mapper.parseEntity(produtoDTO);
		List<Produto> produtos = filterRepository.filtrar(produto);
		return mapper.parseListDTO(produtos);
	}
	
	public List<ProdutoDTO> buscarPorNome(String nome) {
		List<Produto> produtos = repository.findByNome(nome);
	
		return mapper.parseListDTO(produtos);
	}
	
	public ProdutoDTO buscarUm(Long id) {
		Optional<Produto> produtoOp = repository.findById(id);
		if(produtoOp.isPresent()) {
			Produto produto = produtoOp.get();
			return mapper.parseDTO(produto);
		}
		
		throw new EntityNotFoundException();
	}
	
	@Transactional
	public ProdutoDTO criar(ProdutoDTO produtoDTO) {
		Produto produto = mapper.parseEntity(produtoDTO);
		produto.setId(null);
		repository.save(produto);
		em.refresh(produto);
		return mapper.parseDTO(produto);
	}
	
	@Transactional
	public List<ProdutoDTO> criarVarios(List<ProdutoDTO> produtosDTO) {
		List<Produto> produtos = mapper.parseListEntity(produtosDTO);
		repository.saveAll(produtos);
		return mapper.parseListDTO(produtos);
	}
	
	public ProdutoDTO editar(Long id, ProdutoDTO produtoDTO) {
		
		if(repository.existsById(id)) {
			Produto produto = mapper.parseEntity(produtoDTO);
			produto.setId(id);
			produto = repository.save(produto);
			return mapper.parseDTO(produto);
		}
		
		throw new EntityNotFoundException();
	}
	
	public void excluir(Long id) {
		if(!repository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		
		repository.deleteById(id);
	}
}
