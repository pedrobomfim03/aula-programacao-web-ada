package tech.ada.aula.web.service;

import tech.ada.aula.web.model.dto.TokenDTO;
import tech.ada.aula.web.model.dto.UsuarioLoginDTO;

public interface UsuarioService extends BaseService<UsuarioLoginDTO>{
	TokenDTO logar(UsuarioLoginDTO usuarioLoginDTO) throws Exception;
	TokenDTO atualizarToken(String refreshToken);
}
