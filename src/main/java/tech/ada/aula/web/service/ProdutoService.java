package tech.ada.aula.web.service;

import java.util.List;

import tech.ada.aula.web.model.dto.ProdutoDTO;

public interface ProdutoService extends BaseService<ProdutoDTO>{
	List<ProdutoDTO> buscarPorNome(String nome);
	List<ProdutoDTO> criarVarios(List<ProdutoDTO> produtosDTO);
	List<ProdutoDTO> filter(ProdutoDTO produtoDTO);
}
