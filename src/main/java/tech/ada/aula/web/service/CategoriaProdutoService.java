package tech.ada.aula.web.service;

import tech.ada.aula.web.model.dto.CategoriaProdutoDTO;

public interface CategoriaProdutoService extends BaseService<CategoriaProdutoDTO>{

}
