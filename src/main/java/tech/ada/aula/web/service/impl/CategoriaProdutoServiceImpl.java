package tech.ada.aula.web.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import tech.ada.aula.web.model.dto.CategoriaProdutoDTO;
import tech.ada.aula.web.model.entity.CategoriaProduto;
import tech.ada.aula.web.model.mapper.CategoriaProdutoMapper;
import tech.ada.aula.web.repository.CategoriaProdutoRepository;
import tech.ada.aula.web.service.CategoriaProdutoService;

@Service
public class CategoriaProdutoServiceImpl implements CategoriaProdutoService {

	@Autowired
	private CategoriaProdutoRepository repository;
	
	@Autowired
	private CategoriaProdutoMapper mapper;
	
	public List<CategoriaProdutoDTO> buscarTodos() {
		return mapper.parseListDTO(repository.findAll());
	}
	
	public CategoriaProdutoDTO buscarUm(Long id) {
		Optional<CategoriaProduto> categoriaProdutoOp = repository.findById(id);
		if(categoriaProdutoOp.isPresent()) {
			CategoriaProduto categoria = categoriaProdutoOp.get();
			return mapper.parseDTO(categoria);
		}
		
		throw new EntityNotFoundException();
	}
	
	public CategoriaProdutoDTO criar(CategoriaProdutoDTO categoriaProdutoDTO) {
		CategoriaProduto categoriaProduto = mapper.parseEntity(categoriaProdutoDTO);
		categoriaProduto.setId(null);
		repository.save(categoriaProduto);
		return mapper.parseDTO(categoriaProduto);
	}
	
	public CategoriaProdutoDTO editar(Long id, CategoriaProdutoDTO categoriaProdutoDTO) {
		
		if(repository.existsById(id)) {
			CategoriaProduto categoriaProduto = mapper.parseEntity(categoriaProdutoDTO);
			categoriaProduto.setId(id);
			categoriaProduto = repository.save(categoriaProduto);
			return mapper.parseDTO(categoriaProduto);
		}
		
		throw new EntityNotFoundException();
	}
	
	public void excluir(Long id) {
		if(!repository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		
		repository.deleteById(id);
	}
}
