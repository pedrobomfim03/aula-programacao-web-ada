package tech.ada.aula.web.service.impl;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import jakarta.persistence.EntityNotFoundException;
import tech.ada.aula.web.model.dto.ClienteDTO;
import tech.ada.aula.web.model.dto.LocalidadeDTO;
import tech.ada.aula.web.model.entity.Cliente;
import tech.ada.aula.web.model.mapper.ClienteMapper;
import tech.ada.aula.web.repository.ClienteRepository;
import tech.ada.aula.web.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{

	@Autowired
	private ClienteRepository repository;
	
	@Autowired
	private ClienteMapper mapper;
	
	private RestTemplate restTemplate = new RestTemplate();
	
	@Value("${api.viacep}")
	private String urlViaCep;
	
	public List<ClienteDTO> buscarTodos() {
		return mapper.parseListDTO(repository.findAll());
	}
	
	public ClienteDTO buscarUm(Long id) {
		Optional<Cliente> clienteOp = repository.findById(id);
		if(clienteOp.isPresent()) {
			Cliente cliente = clienteOp.get();
			return mapper.parseDTO(cliente);
		}
		
		throw new EntityNotFoundException();
	}
	
	public ClienteDTO criar(ClienteDTO clienteDTO) {
		
		ResponseEntity<LocalidadeDTO> response = restTemplate.getForEntity(
				URI.create(urlViaCep+clienteDTO.getCep()+"/json/"), LocalidadeDTO.class);
		
		LocalidadeDTO localidade = response.getBody();
		
		Cliente cliente = mapper.parseEntity(clienteDTO);
		
		cliente.setBairro(localidade.getBairro());
		cliente.setCep(localidade.getCep());
		cliente.setLocalidade(localidade.getLogradouro());
		cliente.setMunicipio(localidade.getLocalidade());
		cliente.setUf(localidade.getUf());
		
		cliente.setId(null);
		repository.save(cliente);
		return mapper.parseDTO(cliente);
	}
	
	public ClienteDTO editar(Long id, ClienteDTO clienteDTO) {
		
		if(repository.existsById(id)) {
			Cliente cliente = mapper.parseEntity(clienteDTO);
			cliente.setId(id);
			cliente = repository.save(cliente);
			return mapper.parseDTO(cliente);
		}
		
		throw new EntityNotFoundException();
	}
	
	public void excluir(Long id) {
		if(!repository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		
		repository.deleteById(id);
	}
}
