package tech.ada.aula.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import tech.ada.aula.web.model.entity.Perfil;
import tech.ada.aula.web.repository.PerfilRepository;

@Configuration
public class InitialDataConfig {

	@Autowired
	private PerfilRepository repository;
	
	@Bean
	public void inserirDados() {
		Perfil perfilUm = Perfil.builder().nome("ROLE_ADMIN").descricao("Perfil de administrador").build();
		Perfil perfilDois = Perfil.builder().nome("ROLE_USER").descricao("Perfil de usuário comum").build();
		
		repository.save(perfilUm);
		repository.save(perfilDois);
	}
}
